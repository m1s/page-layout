const calendars = [document.getElementById('calendar1'), document.getElementById('calendar2'), document.getElementById('calendar3')];

function createCalendar(element, month, year) {
    const daysInMonth = new Date(year, month + 1, 0).getDate();
    const firstDayOfMonth = new Date(year, month, 1).getDay();

    const monthNames = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
    const dayNames = [, 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс']; // Названия дней недели

    element.innerHTML = `
        <div class="month-header">${monthNames[month]} ${year}</div>
        <div class="days">
            ${dayNames.map(day => `<div class="day">${day}</div>`).join('')} <!-- Добавляем блок с названиями дней недели -->
            ${generateDays(month, year, daysInMonth, firstDayOfMonth)}
        </div>
    `;
}

function generateDays(month, year, daysInMonth, firstDayOfMonth) {
    let daysHtml = '';
    let dayCount = 1;
    const totalDays = 42;

    const firstDayOfPrevMonth = new Date(year, month, 0).getDay();
    const daysInPrevMonth = new Date(year, month, 0).getDate();

    for (let i = firstDayOfPrevMonth; i > 0; i--) {
        daysHtml += `<div class="day disabled">${daysInPrevMonth - i + 1}</div>`;
        dayCount++;
    }

    for (let i = 1; i <= daysInMonth; i++) {
        daysHtml += `<div class="day">${i}</div>`;
        dayCount++;
    }

    const daysInNextMonth = 35 - dayCount + 1;
    for (let i = 1; i <= daysInNextMonth; i++) {
        daysHtml += `<div class="day disabled">${i}</div>`;
        dayCount++;
    }

    return daysHtml;
}

function updateCalendar(element, month, year) {
    createCalendar(element, month, year);
}

const currentMonth = new Date().getMonth();
const currentYear = new Date().getFullYear();
calendars.forEach(calendar => createCalendar(calendar, currentMonth, currentYear));

document.querySelectorAll('.month-link').forEach(link => {
    link.addEventListener('click', (event) => {
        event.preventDefault();
        const month = parseInt(event.target.getAttribute('data-month'), 10);
        calendars.forEach(calendar => updateCalendar(calendar, month, currentYear));
    });
});